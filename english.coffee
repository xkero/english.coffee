# English, Coffeescripter! Do you speak it!?

exports.pad = (num) -> if String(num).length < 2 then "0#{num}" else "#{num}"

exports.timeSince = (start, end=new Date()) -> exports.duration Math.floor (end - start) / 1000

exports.duration = (seconds) ->
	steps =
		years:  31536000
		months:  2592000
		days:      86400
		hours:      3600
		minutes:      60
		seconds:       1
	result = {}
	for name of steps
		interval = Math.floor seconds / steps[name]
		seconds -= interval * steps[name]
		result[name] = interval
	return result

exports.sentence = (object) -> {}

exports.capitalise = (string) -> string.charAt(0).toUpperCase() + string.slice 1

exports.months = [
	'january'
	'february'
	'march'
	'april'
	'may'
	'june'
	'july'
	'august'
	'september'
	'october'
	'november'
	'december'
]

exports.days = [
	'sunday'
	'monday'
	'tuesday'
	'wednesday'
	'thursday'
	'friday'
	'saturday'
]

exports.ordinals = [
	'th'
	'st'
	'nd'
	'rd'
]

exports.ordinalise = (cardinal) ->
	number = String cardinal
	ordinal = (if cardinal in [11, 12, 13] then exports.ordinals[0]
	else
		unit = number[number.length-1]
		if exports.ordinals[unit]? then exports.ordinals[unit]
		else exports.ordinals[0])
	number+ordinal

exports.fullDate = (date=new Date()) ->
	day   = exports.capitalise exports.days[date.getDay()]
	month = exports.capitalise exports.months[date.getMonth()]
	year  = date.getFullYear()
	"#{day} #{exports.ordinalise date.getDay()} #{month}, #{year}"
